<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUsersData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $users = [
            [
                'name' => 'John Doe',
                'gender' => true
            ],
            [
                'name' => 'Jane Nord',
                'gender' => false
            ],
            [
                'name' => 'Noisie Nimad',
                'gender' => false
            ],
            [
                'name' => 'Marta Ogirera',
                'gender' => false
            ],
            [
                'name' => 'Uriayh Faber',
                'gender' => true
            ],
            [
                'name' => 'Alan Smith',
                'gender' => true
            ],
            [
                'name' => 'James Gunn',
                'gender' => true
            ],
            [
                'name' => 'Vasya Pupkin',
                'gender' => true
            ],
            [
                'name' => 'Sidorov Ivan',
                'gender' => true
            ],
            [
                'name' => 'Jackie Chan',
                'gender' => true
            ],
            [
                'name' => 'Masha Lyskova',
                'gender' => false
            ],
            [
                'name' => 'Pavel Nedned',
                'gender' => true
            ],
            [
                'name' => 'Lionel Messi',
                'gender' => true
            ],
            [
                'name' => 'Cristiano Ronaldo',
                'gender' => true
            ],
            [
                'name' => 'Milan Baros',
                'gender' => true
            ],
            [
                'name' => 'Luis Garsia',
                'gender' => true
            ],
            [
                'name' => 'Jennifer Lopez',
                'gender' => false
            ],
            [
                'name' => 'Ketty Perry',
                'gender' => false
            ],
            [
                'name' => 'Katya Lel',
                'gender' => false
            ],
            [
                'name' => 'Demie Rose',
                'gender' => false
            ]
        ];

        for ($i = 0; $i < 20; $i++) {
            $user = new User();
            $time = rand(25704546, 1512933376);
            $date_string = date("Y-m-d H:i:s", $time);

            $user->setUsername('user' . $i)
                ->setEmail('user' . $i . '@mail.ru')
                ->setFullName($users[$i]['name'])
                ->setGender($users[$i]['gender'])
                ->setBirthday(new \DateTime($date_string))
                ->setRoles(['ROLE_USER'])
                ->setEnabled(true);
            $encoder = $this->container->get('security.password_encoder');
            $password = $encoder->encodePassword($user, '123');
            $user->setPassword($password);

            $manager->persist($user);
        }

        $manager->flush();
    }
}