<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Form\ArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ArticlesController
 * @see Route   - добавляем просто для того, чтобы в use с этими
 * @see Method  - классами не подсвечивался как неиспользуемый
 * @package AppBundle\Controller
 */
class ArticlesController extends Controller
{
    /**
     * @Route("/articles/new")
     * @Method({"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $article = new Article();

        $form = $this->createForm(ArticleType::class, $article, [
            'method' => 'POST'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Article $article */
            $article = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute("app_articles_index", ['id' => $article->getId()] );
        }

        return $this->render('AppBundle:Articles:edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/")
     * @Method({"GET","HEAD"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $articles = $this->getDoctrine()
            ->getRepository('AppBundle:Article')
            ->getLastArticles();

        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $paginate_article =  $paginator->paginate(
            $articles,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 3)
        );

        $form = $this->createForm(ArticleType::class, null, [
            'method' => 'POST',
            'action' => $this->generateUrl('app_articles_new')
        ]);

        return $this->render('@App/Articles/index.html.twig', [
            'new_form' => $form->createView(),
            'Articles' => $paginate_article
        ]);
    }

    /**
     * @Route("/articles/{id}/edit", requirements={"id": "\d+"})
     * @Method({"GET","HEAD", "PUT"})
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, int $id)
    {
        $article = $this->getDoctrine()
            ->getRepository('AppBundle:Article')
            ->find($id);

        $form = $this->createForm(ArticleType::class, $article, [
            'method' => 'PUT'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article->setCreatedAt(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute("app_articles_index");
        }

        return $this->render('AppBundle:Articles:edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/articles/{id}/remove", requirements={"id": "\d+"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeAction(int $id)
    {
        $article = $this->getDoctrine()
            ->getRepository('AppBundle:Article')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($article);
        $em->flush();

        return $this->redirectToRoute("app_articles_index");
    }
}