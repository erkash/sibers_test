<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('full_name', TextType::class)
                ->add('gender', ChoiceType::class, [
                    'choices'  => [
                        'Male' => true,
                        'Female' => false,
                        'None' => null
                    ],
                    'expanded' => true,
                    'multiple' => false
                ])
                ->add('birthday', BirthdayType::class)
            ->add( 'register',SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary btn-lg'
                ]]);
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}