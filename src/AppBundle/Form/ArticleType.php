<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    const LABELS = ['POST' => 'Добавить', 'PUT' => 'Обновить'];
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
        ]);
        $builder->add('body', TextareaType::class, [
            'attr' => [
                'rows' => 5
            ]
        ]);
        $builder->add('tag', TextType::class, [
            'required' => false
        ]);
        $builder->add('save', SubmitType::class, [
            'label' => self::LABELS[$options['method']],
            'attr' => [
                'class' => 'btn btn-primary'
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_article_type';
    }
}